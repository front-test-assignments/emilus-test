import { useDrag } from 'react-dnd';
import picksSrc from './consts';
import { ItemTypes } from './ItemTypes';
const style = {
    border: '1px dashed gray',
    backgroundColor: 'white',
    padding: '0.5rem 1rem',
    marginRight: '1.5rem',
    marginBottom: '1.5rem',
    marginLeft: '1.5rem',
    cursor: 'move',
    float: 'left',
};
export const Item = function Box({ name, title, buttonClickHandler, picSrc }) {
    const [{ isDragging }, drag] = useDrag(() => ({
        type: ItemTypes.BOX,
        item: { name },
        end: (item, monitor) => {
            const dropResult = monitor.getDropResult();
            console.log(dropResult)
            if (item && dropResult) {
                buttonClickHandler(item.name, picSrc)
            }
        },
        collect: (monitor) => ({
            isDragging: monitor.isDragging(),
            handlerId: monitor.getHandlerId(),
        }),
    }));
    const opacity = isDragging ? 0.4 : 1;
    return (<div ref={drag} role="Box" style={{ ...style, opacity }} data-testid={`box-${name}`}>
        {name}
    </div>);
};