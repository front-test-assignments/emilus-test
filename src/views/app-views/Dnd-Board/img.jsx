import { useDrag } from 'react-dnd';
import { ItemTypes } from './ItemTypes';

const style = {
    position: 'absolute',
    border: '1px dashed gray',
    backgroundColor: 'white',
    padding: '0.5rem 1rem',
    cursor: 'move',
    maxHeight: '100px',
    maxWidth: '100px'
};
export const Img = ({ id, left, top, hideSourceOnDrag, children, src, removeItem }) => {
    console.log(src, 'in img')
    const [{ isDragging }, drag] = useDrag(() => ({
        type: ItemTypes.BOX,
        item: { id, left, top },
        collect: (monitor) => ({
            isDragging: monitor.isDragging(),
        }),
    }), [id, left, top]);
    if (isDragging && hideSourceOnDrag) {
        return <div ref={drag} />;
    }
    return (<img src={src} alt={'test'} ref={drag} style={{ ...style, left, top }} role="Box" onContextMenu={(e) => { e.preventDefault(); removeItem(id) }}></img>);
};