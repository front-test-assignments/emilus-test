import { useCallback } from 'react';
import { useDrop } from 'react-dnd';
import { ItemTypes } from './ItemTypes';
import { Item } from './Item.jsx';
import update from 'immutability-helper';
import { Img } from './img';
import picksSrc from './consts';

const stylesMainContainer = {
  width: 900,
  height: 600,
  border: '1px solid black',
  position: 'relative',
};

const stylesContainer = {
  width: 900,
  height: 600,
  border: '1px solid black',
  position: 'relative',
};

const Container = (props) => {
  const {
    hideSourceOnDrag,
    buttonClickHandler,
    boxes,
    setBoxes,
    removeItem,
  } = props

  const moveBox = useCallback((id, left, top) => {
    if (!id || !left || !top) {
      return
    }
    setBoxes(update(boxes, {
      [id]: {
        $merge: { left, top },
      },
    }));
  }, [boxes, setBoxes]);

  const [{ canDrop, isOver }, drop] = useDrop(() => ({
    accept: ItemTypes.BOX,
    drop(item, monitor) {
      const delta = monitor.getDifferenceFromInitialOffset();
      const left = Math.round(item.left + delta.x);
      const top = Math.round(item.top + delta.y);
      moveBox(item.id, left, top);
      return { name: 'dustbin' };
    },
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  }), [moveBox]);

  // const removeItem = (id) => {
  //   const removeBiId = (id, object) => {
  //     delete object[id]
  //     return object
  //   }
  //   setBoxes(boxes => removeBiId(id, boxes))
  // }

  return (
    <div style={{ display: 'flex', flexDirection: 'row' }}>
      <div ref={drop} role={'Dustbin'} style={stylesMainContainer}>
        {Object.keys(boxes).map((key) => {
          const { left, top, title, pick } = boxes[key];
          console.log(pick, 'in container')
          return (<Img key={key} id={key} left={left} top={top} hideSourceOnDrag={hideSourceOnDrag} src={pick} removeItem={removeItem} >
            {title}
          </Img>);
        })}
      </div>
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <Item name='wall' title='wall' buttonClickHandler={buttonClickHandler} picSrc={picksSrc.chear}>wall</Item>
        <Item name='chear' title='chear' buttonClickHandler={buttonClickHandler} picSrc={picksSrc.chear2}>chear</Item>
        <Item name='table' title='table' buttonClickHandler={buttonClickHandler} picSrc={picksSrc.table}>table</Item>
      </div>
    </div>);
};

export default Container