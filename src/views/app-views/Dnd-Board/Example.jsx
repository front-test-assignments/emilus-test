import { useState, useCallback } from 'react';
import Container from './Container';
import { Upload, Button } from 'antd';
import { UploadOutlined, DownloadOutlined, BlockOutlined } from '@ant-design/icons';
import crypto from 'crypto'

export const Example = () => {
	const [hideSourceOnDrag, setHideSourceOnDrag] = useState(true);
	const [boxes, setBoxes] = useState({
		a: { top: 20, left: 80, title: 'Drag me around' },
		b: { top: 180, left: 20, title: 'Drag me too' },
	});

	const removeById = (id, boxes) => {
		const { [id]: _removedElement, ...newBoxes } = boxes
		return newBoxes
	}

	const removeItem = (id) => setBoxes(boxes => removeById(id, boxes))

	const toggle = useCallback(() => setHideSourceOnDrag(!hideSourceOnDrag), [
		hideSourceOnDrag,
	]);

	const getRandomInt = (max) => {
		return Math.floor(Math.random() * max);
	}

	const getRandomString = () => crypto.randomBytes(3).toString('hex');

	const getRandomBox = (id, pickSrc) => {
		var name = getRandomString()
		const newBox = {}
		const value = {
			top: getRandomInt(500),
			left: getRandomInt(800),
			title: id ? id : getRandomString(),
			pick: pickSrc
		}
		newBox[name] = value
		console.log(newBox)
		return newBox
	}

	const buttonClickHandler = (id, pickSrc) => {
		const newBox = id ? getRandomBox(id, pickSrc) : getRandomBox()
		setBoxes((boxes) => {
			return {
				...boxes,
				...newBox
			}
		})
	}

	const downloadFile = async () => {
		const myData = boxes;
		const fileName = "file";
		const json = JSON.stringify(myData);
		const blob = new Blob([json], { type: 'application/json' });
		const href = await URL.createObjectURL(blob);
		const link = document.createElement('a');
		link.href = href;
		link.download = fileName + ".json";
		document.body.appendChild(link);
		link.click();
		document.body.removeChild(link);
	}

	const downloadedFileHandler = (file) => {
		const reader = new FileReader();

		reader.onload = e => {
			const newBoxes = JSON.parse(e.target.result)
			setBoxes(newBoxes)
		};
		reader.readAsText(file);

		return false;
	}

	return (<div>
		<div style={{ paddingBottom: '10px' }}>
			<Button icon={<BlockOutlined />} onClick={() => buttonClickHandler()}>Add random object</Button>
			<Button icon={<DownloadOutlined />} onClick={() => downloadFile()}>Click to Download your positions of items</Button>
			<Upload
				accept=".txt, .csv"
				showUploadList={false}
				beforeUpload={file => downloadedFileHandler(file)}
			>
				<Button disabled={false} icon={<UploadOutlined />}>Click to Upload current position of items</Button>
			</Upload>
		</div>
		<Container
			hideSourceOnDrag={hideSourceOnDrag}
			buttonClickHandler={buttonClickHandler}
			boxes={boxes}
			setBoxes={setBoxes}
			removeItem={removeItem}
		/>
		<p>
			<label htmlFor="hideSourceOnDrag">
				<input id="hideSourceOnDrag" type="checkbox" role="checkbox" checked={hideSourceOnDrag} onChange={toggle} />
				<small>Hide the source item while dragging</small>
			</label>
		</p>
	</div>);
};