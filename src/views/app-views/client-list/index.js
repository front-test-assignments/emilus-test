import React, { Component } from 'react'
import { connect } from "react-redux";
import { Card, Table, message } from 'antd';
import UserView from './ClientView';
import { addClients } from 'redux/actions';
import { Link } from 'react-router-dom';
import { APP_PREFIX_PATH } from 'configs/AppConfig';

export class ClientList extends Component {

	state = {
		userProfileVisible: false,
		selectedUser: null
	}
	
	componentDidMount = () => {
		fetch('https://jsonplaceholder.typicode.com/users')
		.then(response => response.json())
		.then(json => this.props.addClients(json))
	}

	deleteUser = userId => {
		this.setState({
			users: this.state.users.filter(item => item.id !== userId),
		})
		message.success({ content: `Deleted user ${userId}`, duration: 2 });
	}

	showUserProfile = userInfo => {
		this.setState({
			userProfileVisible: true,
			selectedUser: userInfo
		});
	};
	
	closeUserProfile = () => {
		this.setState({
			userProfileVisible: false,
			selectedUser: null
    });
	}

	render() {
		const { userProfileVisible, selectedUser } = this.state;
		const { clients } = this.props;

		const tableColumns = [
			{
				title: 'User',
				dataIndex: 'name',
				render: (_, record) => (
					<div className="d-flex">
						<Link to={`${APP_PREFIX_PATH}/client/${record.id}/edit`}>{record.name}</Link>
					</div>
				),
				sorter: {
					compare: (a, b) => {
						a = a.name.toLowerCase();
  						b = b.name.toLowerCase();
						return a > b ? -1 : b > a ? 1 : 0;
					},
				},
			},
			{
				title: 'User',
				dataIndex: 'username',
				render: (_, record) => (
					<div className="d-flex">
						<p>{record.username}</p>
					</div>
				),
				sorter: {
					compare: (a, b) => {
						a = a.name.toLowerCase();
  						b = b.name.toLowerCase();
						return a > b ? -1 : b > a ? 1 : 0;
					},
				},

			},
			{
				title: 'Email',
				dataIndex: 'email',
				render: (_, record) => (
					<div className="d-flex">
						<p>{record.email}</p>
					</div>
				),
				sorter: {
					compare: (a, b) => {
						a = a.name.toLowerCase();
  						b = b.name.toLowerCase();
						return a > b ? -1 : b > a ? 1 : 0;
					},
				},

			},
		];
		return (
			<Card bodyStyle={{'padding': '0px'}}>
				<Table columns={tableColumns} dataSource={clients} rowKey='id' />
				<UserView data={selectedUser} visible={userProfileVisible} close={()=> {this.closeUserProfile()}} />
			</Card>
		)
	}
}

const mapStateToProps = ({ clients }) => ({ clients });
const mapDispatchToProps = ({ addClients })

export default connect(mapStateToProps, mapDispatchToProps)(ClientList)