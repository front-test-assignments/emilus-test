import React, { Component } from 'react';
import { Form, Avatar, Button, Input, DatePicker, Row, Col, message, Upload } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { ROW_GUTTER } from 'constants/ThemeConstant';
import Flex from 'components/shared-components/Flex'
import { setCurrentClient } from 'redux/actions';
import { connect } from "react-redux";
import Loading from 'components/shared-components/Loading';
import { APP_PREFIX_PATH } from 'configs/AppConfig';

export class EditProfile extends Component {

	avatarEndpoint = 'https://www.mocky.io/v2/5cc8019d300000980a055e76'

	state= {
		avatarUrl: '/img/avatars/thumb-6.jpg',
		dateOfBirth: null,
    isLoading: false,
    simulateLoading: false
	}

  componentDidMount = () => {
    const id = this.getClientId()
    this.fetchCurrentClient(id)
  }

	getBase64(img, callback) {
		const reader = new FileReader();
		reader.addEventListener('load', () => callback(reader.result));
		reader.readAsDataURL(img);
	}

  onFinish = values => {
    this.setState({simulateLoading: true})
    setTimeout(() => {
      window.location = `${APP_PREFIX_PATH}/client-list`
    }, 1000)
  };

  onFinishFailed = errorInfo => {
  };

  onUploadAavater = info => {
    const key = 'updatable';
    if (info.file.status === 'uploading') {
      message.loading({ content: 'Uploading...', key, duration: 1000 });
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, imageUrl =>
        this.setState({
          avatarUrl: imageUrl,
        }),
      );
      message.success({ content: 'Uploaded!', key,  duration: 1.5 });
    }
  };

  onRemoveAvater = () => {
    this.setState({
      avatarUrl: ''
    })
  }

  getClientId = () => {
    const { id } = this.props.match.params;
    return id;
  }
      
  fetchCurrentClient = (id) => { 
		fetch(`https://jsonplaceholder.typicode.com/users/${id}`)
      .then(response => response.json())
      .then(json => this.props.setCurrentClient(json))
  }

  EditProfilePage = (currentClient) => {			
    const { dateOfBirth, avatarUrl } = this.state;

    return( 
      <>
        <Flex alignItems="center" mobileFlex={false} className="text-center text-md-left">
          <Avatar size={90} src={avatarUrl} icon={<UserOutlined />}/>
          <div className="ml-md-3 mt-md-0 mt-3">
            <Upload onChange={this.onUploadAavater} showUploadList={false} action={this.avatarEndpoint}>
              <Button type="primary">Change Avatar</Button>
            </Upload>
            <Button className="ml-2" onClick={this.onRemoveAvater}>Remove</Button>
          </div>
        </Flex>
        <div className="mt-4">
          <Form
            name="basicInformation"
            layout="vertical"
            initialValues={
              { 
                'name': currentClient.name,
                'email': currentClient.email,
                'username': currentClient.username,
                'dateOfBirth': dateOfBirth,
                'phoneNumber': currentClient.phone,
                'website': currentClient.website,
                'address': currentClient.address.street,
                'city': currentClient.address.city,
                'postcode': currentClient.address.zipcode
              }
            }
            onFinish={this.onFinish}
            onFinishFailed={this.onFinishFailed}
          >
            <Row>
              <Col xs={24} sm={24} md={24} lg={16}>
                <Row gutter={ROW_GUTTER}>
                  <Col xs={24} sm={24} md={12}>
                    <Form.Item
                      label="Name"
                      name="name"
                      rules={[
                        {
                          required: true,
                          message: 'Please input your name!',
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>
                  </Col>
                  <Col xs={24} sm={24} md={12}>
                    <Form.Item
                      label="Username"
                      name="username"
                      rules={[
                        {
                          required: true,
                          message: 'Please input your username!'
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>
                  </Col>
                  <Col xs={24} sm={24} md={12}>
                    <Form.Item
                      label="Email"
                      name="email"
                      rules={[{ 
                        required: true,
                        type: 'email',
                        message: 'Please enter a valid email!' 
                      }]}
                    >
                      <Input />
                    </Form.Item>
                  </Col>
                  <Col xs={24} sm={24} md={12}>
                    <Form.Item
                      label="Date of Birth"
                      name="dateOfBirth"
                    >
                      <DatePicker className="w-100"/>
                    </Form.Item>
                  </Col>
                  <Col xs={24} sm={24} md={12}>
                    <Form.Item
                      label="Phone Number"
                      name="phoneNumber"
                    >
                      <Input />
                    </Form.Item>
                  </Col>
                  <Col xs={24} sm={24} md={12}>
                    <Form.Item
                      label="Website"
                      name="website"
                    >
                      <Input />
                    </Form.Item>
                  </Col>
                  <Col xs={24} sm={24} md={24}>
                    <Form.Item
                      label="Address"
                      name="address"
                    >
                      <Input />
                    </Form.Item>
                  </Col>
                  <Col xs={24} sm={24} md={12}>
                    <Form.Item
                      label="City"
                      name="city"
                    >
                      <Input />
                    </Form.Item>
                  </Col>
                  <Col xs={24} sm={24} md={12}>
                    <Form.Item
                      label="Post code"
                      name="postcode"
                    >
                      <Input />
                    </Form.Item>
                  </Col>
                </Row>
                <Button type="primary" htmlType="submit">
                  Save Change
                </Button>
              </Col>
            </Row>
          </Form>
        </div>
      </>
    )
  } 
  render() {
    const { simulateLoading } = this.state;
    const { currentClient } = this.props;
    const showLoading = !currentClient || simulateLoading
    return  showLoading ?  <Loading/> : this.EditProfilePage(currentClient) 
  }
}

const mapStateToProps = ({ currentClient }) => ({ currentClient });
const mapDispatchToProps = ({ setCurrentClient })

export default connect(mapStateToProps, mapDispatchToProps )(EditProfile)
