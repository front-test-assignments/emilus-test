import { SET_CURRENT_CLIENT } from '../constants/Clients'

const initState = null


const currentClient = (state = initState, action) => {
	switch (action.type) {
		case SET_CURRENT_CLIENT:
			return action.payload
		default:
			return state;
	}
}

export default currentClient