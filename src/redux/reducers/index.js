import { combineReducers } from 'redux';
import Auth from './Auth';
import Theme from './Theme';
import Clients from './Clients';
import CurrentClient from './CurrentClient';

const reducers = combineReducers({
    theme: Theme,
    auth: Auth,
    clients: Clients,
    currentClient: CurrentClient
});

export default reducers;