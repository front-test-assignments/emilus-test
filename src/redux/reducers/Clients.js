import { ADD_CLIENTS } from '../constants/Clients'

const initState = []

const clients = (state = initState, action) => {
	switch (action.type) {
		case ADD_CLIENTS:
			return [
				...action.payload
			]
		default:
			return state;
	}
}

export default clients