import { ADD_CLIENTS, SET_CURRENT_CLIENT } from '../constants/Clients'

export const addClients = (clients) => {
  return {
    type: ADD_CLIENTS,
    payload: clients
  }
};
export const setCurrentClient = (client) => {
  return {
    type: SET_CURRENT_CLIENT,
    payload: client
  }
};
